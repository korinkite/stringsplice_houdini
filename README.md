## How to run
The quickest way is to run a hscript expression, from command-line. I'm not
sure what the best way to do it is but this worked for me:

```
hython -c "print hou.hscriptExpression('stringsplice(\"asfsfd_tttt\", \"1\")')"
```

## Installation Instructions
- Install gcc/g++ and cmake versions (See Tested On section, below).
  Make sure the Houdini version is compiled with the same gcc version.

Then run the following:
```
git clone --recursive -j8 https://korinkite@bitbucket.org/korinkite/stringsplice_houdini.git
cd stringsplice_houdini/build
cmake ..
make
```

Note:
    git clone --recursive requires a fairly new git version. If you're on older
	git, use

	```
	git clone https://korinkite@bitbucket.org/korinkite/stringsplice_houdini.git
	cd stringsplice_houdini
	git submodule --init --recursive
	```

	Both do the same thing

	Pro tip: -j8 does a multi-threaded clone for fastest results. It's optional.

Warning:
    If your Houdini environment is not set up correctly, `cmake ..` will fail
	with an error like this:

	```
	CMake Error at CMakeLists.txt:13 (find_package):
	  By not providing "FindHoudini.cmake" in CMAKE_MODULE_PATH this project has
	  asked CMake to find a package configuration file provided by "Houdini", but
	  CMake did not find one.

	  Could not find a package configuration file provided by "Houdini" with any
	  of the following names:

		HoudiniConfig.cmake
		houdini-config.cmake

	  Add the installation prefix of "Houdini" to CMAKE_PREFIX_PATH or set
	  "Houdini_DIR" to a directory containing one of the above files.  If
	  "Houdini" provides a separate development package or SDK, be sure it has
	  been installed.
	```

	This is expected and normal.  Review the "Compiling with CMake" section
	in this URL.

	https://www.sidefx.com/docs/hdk/_h_d_k__intro__compiling.html


The plugin file will be placed in ~/houdiniX.Y/dso/stringsplice_plugin.so.
You should now be able to use the function as needed.


## Tested On
Centos 7 64 bit
gcc/g++ 4.8.5
Houdini 16.5 (compiled with gcc 4.8)
cmake 3.11


## Todo
- Improve exception handling
- Generate documentation so that hscript's `exhelp` command is more useful
- Research cmake projects to find better ways of hooking up projects
- Remove the function table in command.C (low priority)
- Try to "reserve spaces" for my vector


## Original Brief
```
Custom HScript Commands
2 messages
Cole Clark <colevfx@gmail.com>	Thu, May 3, 2018 at 10:04 AM
To: Colin Kennedy <colinvfx@gmail.com>
Node Type: Agent Layer Sop
OS: propLayer_male_swordShortA_right
Parm: layername (str)

The goal is to have the easiest way of referencing part of the name with an array slice. In this case, we are picking the node to operate on, and the

My current technique is call inline python from hscript parameter:
`pythonexprs("'_'.join(hou.pwd().name().split('_')[2:])")`
Pythonexprs only supports one line, and I don't think supports imports.

This is equivalent to a python parameter expression:
nameArray = hou.pwd().name.split('_')
return '_'.join(nameArray[2:])
One issue with this technique is that the parameter needs to be set to python script, which by default it is not. It can be made to be the default of course, but most Houdini artists don't know or bother.

My ideal usage I think is to have something that runs in hscript, but the implementation can be python or C++ under the hood.
stringsplice(".", "2:")
(note that Houdini's convention with hscript is all lowercase.)

This is going to be my first C++ project I think because I don't think you can actually implement directly in python. The goal is to make this as concise as possible.

--
Cole Clark
Crowd Technical Director

Website  : coleclark.com
LinkedIn : https://www.linkedin.com/in/colevfx

Cole Clark <colevfx@gmail.com>	Thu, May 3, 2018 at 10:07 AM
To: Colin Kennedy <colinvfx@gmail.com>
Related Documentation:

C++:
http://www.sidefx.com/docs/hdk/_h_d_k__extend_e_x_p_r.html

Python:
http://www.sidefx.com/docs/houdini16.0/hom/expressions
[Quoted text hidden]
